package com.bsevryukov.service;

import com.bsevryukov.domain.Quote;
import com.bsevryukov.repository.QuoteRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class QuoteServiceIT {
    @Autowired
    private QuoteRepository quoteRepository;

    @Autowired
    private QuoteService quoteService;

    @BeforeEach
    void setup() {
        quoteRepository.deleteAll();
    }

    @AfterEach
    void tearDown() {
    }

    /*
    @Test
    void save() {
    }

    @Test
    void findAll() {
    }

    @Test
    void findLast() {
    }

    @Test
    void findById() {
    }

    @Test
    void delete() {
    }
    }*/
}
