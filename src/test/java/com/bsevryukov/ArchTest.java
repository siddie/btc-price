package com.bsevryukov;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("com.bsevryukov");

        noClasses()
            .that()
            .resideInAnyPackage("com.bsevryukov.service..")
            .or()
            .resideInAnyPackage("com.bsevryukov.repository..")
            .should().dependOnClassesThat()
            .resideInAnyPackage("..com.bsevryukov.web..")
            .because("Services and repositories should not depend on web layer")
            .check(importedClasses);
    }
}
