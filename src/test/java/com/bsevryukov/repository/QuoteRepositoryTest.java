package com.bsevryukov.repository;

import com.bsevryukov.BtcPriceApp;
import com.bsevryukov.domain.Quote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
//import static com.bsevryukov.repository.CustomAuditEventRepository.EVENT_DATA_COLUMN_MAX_LENGTH;

/**
 * Integration tests for {@link QuoteRepository}.
 */
@SpringBootTest(classes = BtcPriceApp.class)
public class QuoteRepositoryTest {
    private static final ZonedDateTime DEFAULT_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Float DEFAULT_VALUE = 1F;
    private static final Float UPDATED_VALUE = 2F;

    @Autowired
    private QuoteRepository quoteRepository;

    private Quote quote;

    @BeforeEach
    public void setup() {
        quoteRepository.deleteAll();
        quote = new Quote()
            .time(DEFAULT_TIME)
            .value(DEFAULT_VALUE);
    }

    @Test
    public void addQuote() {
        quoteRepository.save(quote);

        List<Quote> persistentQuotes = quoteRepository.findAll();
        assertThat(persistentQuotes).hasSize(1);

        Quote persistentQuote = persistentQuotes.get(0);
        assertThat(persistentQuote.getTime().truncatedTo(ChronoUnit.MILLIS)).isEqualTo(DEFAULT_TIME.truncatedTo(ChronoUnit.MILLIS));
        assertThat(persistentQuote.getValue()).isEqualTo(DEFAULT_VALUE);
    }

    @Test
    public void getAllQuotes() throws Exception {
        // Initialize the database
        quoteRepository.save(quote);
        quoteRepository.save(new Quote()
            .time(DEFAULT_TIME.plusSeconds(1))
            .value(DEFAULT_VALUE + 1));
        quoteRepository.save(new Quote()
            .time(DEFAULT_TIME.plusSeconds(2))
            .value(DEFAULT_VALUE + 2));

        List<Quote> persistentQuotes = quoteRepository.findAll();
        assertThat(persistentQuotes).hasSize(3);
    }

    /*
    @Test
    void findFirstByOrderByTimeDesc() {
    }

    @Test
    void findAll() {
    }

    @Test
    void findAllByTimeBetween() {
    }

    @Test
    void findAllByTimeBefore() {
    }

    @Test
    void findAllByTimeAfter() {
    }
    */
}
