package com.bsevryukov.worker;

import com.bsevryukov.service.QuoteService;
import com.bsevryukov.service.dto.QuoteDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

@Component
public class QuoteDataIntegrationWorker {
    private static final Logger log = LoggerFactory.getLogger(QuoteDataIntegrationWorker.class);
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    private final QuoteService quoteService;

    public QuoteDataIntegrationWorker(QuoteService quoteService) {
        this.quoteService = quoteService;
    }

    @Scheduled(fixedRateString = "#{${application.pollRateSec} * 1000}")
    public void fetchAndStoreBtcUsdQuote() throws JsonProcessingException {
        QuoteDTO quoteDTO = tryFetchBtcUsdQuote();
        log.info(String.valueOf(quoteDTO));

        // writing to DB
        // FIXME: no error handling here
        quoteService.save(quoteDTO);
    }

    /**
     * Limitations: please check how Backoff * maxAttempts plays with pollRateSec
     * TODO: might want to catch a more concrete exception family
     * @return
     */
    @Retryable(value = Exception.class, maxAttempts = 2, backoff = @Backoff(delay = 3000))
    public QuoteDTO tryFetchBtcUsdQuote() throws JsonProcessingException {
        RestTemplate restTemplate = new RestTemplate();

        /*
        Example:
        {
          "result": {
            "price": 56325
          },
          "allowance": {
            "cost": 0.005,
            "remaining": 9.98,
            "upgrade": "For unlimited API access, create an account at https://cryptowat.ch"
          }
        }
         */

        Date now = new Date();
        log.info("Requesting at {}", dateFormat.format(now));

        String upd = restTemplate.getForObject("https://api.cryptowat.ch/markets/bitfinex/btcusd/price", String.class);
        log.info(upd);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(upd);
        float val = (float) root.path("result").path("price").asDouble();
        log.info(String.valueOf(val));

        QuoteDTO quoteDTO = new QuoteDTO();
        ZonedDateTime nowZoned = ZonedDateTime.ofInstant(now.toInstant(), ZoneId.systemDefault());
        quoteDTO.setTime(nowZoned);
        quoteDTO.setValue(val);

        return quoteDTO;
    }

    @Recover
    public QuoteDTO recover(Throwable e, final String str) {
        log.error("{}", e);
        return null;
    }
}
