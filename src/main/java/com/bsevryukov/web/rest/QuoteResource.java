package com.bsevryukov.web.rest;

import com.bsevryukov.service.QuoteService;
import com.bsevryukov.service.dto.QuoteDTO;

import com.bsevryukov.web.rest.util.HeaderUtil;
import com.bsevryukov.web.rest.util.ResponseUtil;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.bsevryukov.domain.Quote}.
 */
@RestController
@RequestMapping("/api")
public class QuoteResource {

    private final Logger log = LoggerFactory.getLogger(QuoteResource.class);

    private final QuoteService quoteService;

    public QuoteResource(QuoteService quoteService) {
        this.quoteService = quoteService;
    }


    /**
     * {@code GET  /quotes} : get all the quotes.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of quotes in body.
     */

    @GetMapping("/quotes")
    @RateLimiter(name = "VERY_LOW")
    public ResponseEntity<List<QuoteDTO>> getAllQuotes(
            @RequestParam(value = "start", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
                    ZonedDateTime startDate,
            @RequestParam(value = "end", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
                    ZonedDateTime endDate,
            Pageable pageable) {

        log.debug("REST request to get a page of Quotes");

        Page<QuoteDTO> page = quoteService.findAll(startDate, endDate, pageable);
        HttpHeaders headers =
                HeaderUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);

        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /} : get the last quote.
     *
     * @return last quote
     */
    @GetMapping("/")
    @RateLimiter(name = "HIGH")
    public ResponseEntity<QuoteDTO> getQuote() {
        log.debug("REST request to get last Quote");
        Optional<QuoteDTO> quoteDTO = quoteService.findLast();
        return ResponseUtil.wrapOrNotFound(quoteDTO);
    }

    /**
     * {@code GET  /quotes/{id}} : get the quote by ID.
     *
     * @return found quote
     */
    @GetMapping("/quotes/{id}")
    @RateLimiter(name = "HIGH")
    public ResponseEntity<QuoteDTO> getQuoteById(@PathVariable String id) {
        log.debug("REST request to get quote by ID");
        Optional<QuoteDTO> quoteDTO = quoteService.findById(id);
        return ResponseUtil.wrapOrNotFound(quoteDTO);
    }
}
