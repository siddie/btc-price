package com.bsevryukov.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Btc Price.
 * FIXME: remove this one as we can live with application.yml only
 * <p>
 * Properties are configured in the {@code application.yml} file.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {
    private int pollRateSec;

    public int getPollRateSec() {
        return pollRateSec;
    }

    public void setPollRateSec(int pollRateSec) {
        this.pollRateSec = pollRateSec;
    }
}
