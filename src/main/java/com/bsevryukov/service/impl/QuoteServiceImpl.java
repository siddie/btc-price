package com.bsevryukov.service.impl;

import com.bsevryukov.service.QuoteService;
import com.bsevryukov.domain.Quote;
import com.bsevryukov.repository.QuoteRepository;
import com.bsevryukov.service.dto.QuoteDTO;
import com.bsevryukov.service.mapper.QuoteMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Quote}.
 */
@Service
public class QuoteServiceImpl implements QuoteService {

    private final Logger log = LoggerFactory.getLogger(QuoteServiceImpl.class);

    private final QuoteRepository quoteRepository;

    private final QuoteMapper quoteMapper;

    public QuoteServiceImpl(QuoteRepository quoteRepository, QuoteMapper quoteMapper) {
        this.quoteRepository = quoteRepository;
        this.quoteMapper = quoteMapper;
    }

    @Override
    public QuoteDTO save(QuoteDTO quoteDTO) {
        log.debug("Request to save Quote : {}", quoteDTO);
        Quote quote = quoteMapper.toEntity(quoteDTO);
        quote = quoteRepository.save(quote);
        return quoteMapper.toDto(quote);
    }

    @Override
    public Page<QuoteDTO> findAll(ZonedDateTime startDate, ZonedDateTime endDate, Pageable pageable) {
        log.debug("Request to get all Quotes");
        if (null != startDate && null != endDate) {
            return quoteRepository.findAllByTimeBetween(startDate, endDate, pageable)
                .map(quoteMapper::toDto);
        } else if (null != startDate) {
            return quoteRepository.findAllByTimeAfter(startDate, pageable)
                .map(quoteMapper::toDto);
        } else if (null != endDate) {
            return quoteRepository.findAllByTimeBefore(endDate, pageable)
                .map(quoteMapper::toDto);
        }

        return quoteRepository.findAll(pageable)
            .map(quoteMapper::toDto);
    }

    @Override
    public Optional<QuoteDTO> findLast() {
        log.debug("Request to get last Quote");
        return quoteRepository.findFirstByOrderByTimeDesc()
            .map(quoteMapper::toDto);
    }

    @Override
    public Optional<QuoteDTO> findById(String id) {
        log.debug("Request to get quote by ID");
        return quoteRepository.findById(id)
            .map(quoteMapper::toDto);
    }

    @Override
    public void delete(String id) {
        log.debug("Request to delete Quote : {}", id);
        quoteRepository.deleteById(id);
    }
}
