package com.bsevryukov.service;

import com.bsevryukov.service.dto.QuoteDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.ZonedDateTime;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.bsevryukov.domain.Quote}.
 */
public interface QuoteService {

    /**
     * Save a quote.
     *
     * @param quoteDTO the entity to save.
     * @return the persisted entity.
     */
    QuoteDTO save(QuoteDTO quoteDTO);

    /**
     * Get all the quotes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<QuoteDTO> findAll(ZonedDateTime startDate, ZonedDateTime endDate, Pageable pageable);

    /**
     * Find last quote
     *
     * @return last quote
     */
    Optional<QuoteDTO> findLast();

    /**
     * Find quote by ID
     *
     * @return found quote
     */
    Optional<QuoteDTO> findById(String id);

    /**
     * Delete the "id" quote.
     *
     * @param id the id of the entity.
     */
    void delete(String id);
}
