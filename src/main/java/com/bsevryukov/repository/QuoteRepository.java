package com.bsevryukov.repository;

import com.bsevryukov.domain.Quote;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.Optional;

/**
 * Spring Data MongoDB repository for the Quote entity.
 */
@SuppressWarnings("unused")
@Repository
public interface QuoteRepository extends MongoRepository<Quote, String> {
    Optional<Quote> findFirstByOrderByTimeDesc();

    Page<Quote> findAll(Pageable pageable);
    Page<Quote> findAllByTimeBetween(ZonedDateTime start, ZonedDateTime end, Pageable pageable);
    Page<Quote> findAllByTimeBefore(ZonedDateTime date, Pageable pageable);
    Page<Quote> findAllByTimeAfter(ZonedDateTime date, Pageable pageable);
}
