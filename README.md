# BtcPrice

A simple app that demonstrates some handy tools from the Spring Boot ecosystem.

Fetches BTCUSD rates via Cryptowatch API. Polling period is configurable via application.yml and can be passed to the svc.

Polling period is defined in seconds. Default value set via the application.pollRateSec of application.yml and configurabel as a param, see below.

Note: that your Cryptowatch API call budget will exhaust rather quickly when setting this to a lower value. The service does not adjust call rate and will rather log errors.

## Implementation details

* This is a monolith app. It contains a data integration worker (writer) and a readonly API.
  This is a demo. In more complex scenarios you might want to decouple the worker and readonly API into two separate services. Further, you might want to evolve to event-driven.

* API. The entry point is the QuoteResource controller, which uses the QuoteService which talks to the QuoteRepository (encapsulating the DB). The Quote domain entity (model) is decoupled from the QuoteDTO. Read and write DTOs are the same. ID should be removed from the DTO.

* It requires MongoDB installed (docker compose for local and k8s configs for prod to be added).

* To remove boilerplate code you could add Lombok - if you are on that side of camp.

* For production, you might want to remove/filter some logging statements.

* The app is about fetching BTCUSD quotes only - so in model/DTO there will be no fields like "symbol" or "pair". Code entity names will omit the "BtcUsd" part.

* TODO: Swagger/OpenAPI documentation and API first.

* TODO: Test coverage fo 70-80.

* FIXME: Remove ID from DTO - it is not needed

* FIXME: DTOs could have unix timestamps. The time strings we have now ("2021-02-20T04:00:29.765+01:00") are handy for debugging.

* TODO: index on timestamp via separate scripts.

* TODO: you might want to add Liquibase to handle DB evolution.

* TODO: review and add missing http codes

* TODO: add protected methods requiring authentication and authorization to demo those.

### Auth
Information (BTC quotes) does not have an owner and is public. Thus, no authentication and authorization is needed to read it.

### Rate Limiting
The thing that seems to be necessary here is rate limiting (throttling). I demonstrated how to implement it on a service instance level.

Having it helps not only to reduce effect of malicious attacks but also to implement systems with backpressure - for the good of everyone in a chain of services.

Note, in large scale setups you would like to have rate limiting on the outer level: e.g. load balancers for the multi-instance setup within the protected net or API gateways at the boundary. If your webapp or other resource is in single instance but is public, put it behind a proxy (e.g. Nginx, Cloudflare).

Please also note that the current implementation is ego-centered. The quota is shared between users.

## Running and building

### Prerequisites
* Gradle
* MongoDB. By default, localhost:27017, no auth. Change in app configs.
* (optional) SonarQube server at http://localhost:9000

### Development
To start in the dev profile, run:

```
./gradlew
```

### Production
To build and run in production mode:
```
./gradlew -Pprod clean bootJar
java -jar build/libs/*.jar --application.pollRateSec=10
```

## Testing
* Unit and integration tests are present (JUnit, Mockito). TODO: coverage could be inproved.
* Additionally, upon importing the [Postman collection](postman/BtcPriceApp.postman_collection.json), you could setup a Postman monitor and thus obtain periodical E2E and functional tests.
  Note: current collection rather provides examples of usage. Some work needs to be done to build a E2E suit with good coverage.
  
## Api usage

Import the Postman collection from [postman/BtcPriceApp.postman_collection.json](postman/BtcPriceApp.postman_collection.json) or check the call examples below:

Get last price:
http://localhost:8080/api/

Get history, range:
http://localhost:8080/api/quotes?start=2021-02-20T10:13:36.917Z&end=2021-02-20T23:16:44.882Z&page=2&size=100&sort=time,desc

Get history, before:
http://localhost:8080/api/quotes?end=2021-02-20T23:16:44.882Z&page=2&size=100&sort=time,desc

Get history, after:
http://localhost:8080/api/quotes?start=2021-02-20T10:13:36.917Z&page=2&size=100&sort=time,desc

### Auth
None

### Pagination
*Page*, *size* and *sort* parameters from the examples below are self-explanatory.
The total size of the list is returned in '*X-Total-Count*' header.

The '*Link*' header will give you links as in http://tools.ietf.org/html/rfc5988. Normally you do not need this information since you explicitly pass the page size.


